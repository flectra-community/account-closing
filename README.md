# Flectra Community / account-closing

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_invoice_start_end_dates](account_invoice_start_end_dates/) | 2.0.1.1.0| Adds start/end dates on invoice/move lines
[account_cutoff_base](account_cutoff_base/) | 2.0.1.2.1| Base module for Account Cut-offs
[account_cutoff_accrual_subscription](account_cutoff_accrual_subscription/) | 2.0.1.0.0| Accrued expenses based on subscriptions
[account_cutoff_accrual_picking](account_cutoff_accrual_picking/) | 2.0.1.1.0| Accrued expense & accrued revenue from pickings
[account_multicurrency_revaluation](account_multicurrency_revaluation/) | 2.0.1.0.0| Manage revaluation for multicurrency environment
[account_cutoff_start_end_dates](account_cutoff_start_end_dates/) | 2.0.1.0.0| Cutoffs based on start/end dates


